var express = require('express');
var router = express.Router();
var fs = require('fs');
router.post('/file-upload', function(req, res) {
    console.log(req.body);	
    var tmp_path = './img/';
    var target_path = './public/images/' + req.files.thumbnail.name;
    fs.rename(tmp_path, target_path, function(err) {
        if (err) throw err;
        fs.unlink(tmp_path, function() {
            if (err) throw err;
            res.send('File uploaded to: ' + target_path + ' - ' + req.files.thumbnail.size + ' bytes');
        });
    });
});
module.exports = router;